#define ledinterno 13
#define ledbranco 5
#define botao 3
int foto_sensor = A0;
int luz;
int sensor;
#define ledbranco2 6

void setup() {
  pinMode(ledinterno, OUTPUT);
  pinMode(ledbranco, OUTPUT);
  pinMode(botao, INPUT);
  Serial.begin(9600);
}

void loop() {
  if(digitalRead(botao) == HIGH) digitalWrite(ledbranco, !digitalRead(ledbranco));
  while(digitalRead(botao) == HIGH){}
  
  sensor = analogRead(foto_sensor);
  
  if (sensor <= 600)
    luz = 0;
  else
    luz = map(sensor, 601,1024, 1,60);
  
  Serial.println(luz);
  
  analogWrite(ledbranco2, luz);
}
